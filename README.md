# Bravoure - Block Slide CSS

## SCSS: Code to be added:

        @import 'jello_components/angular-block-slide-css/block-slide-css';

## Use

It provides the generic css code for using the slides

## Extend

Just duplicate structure inside the jello_components_extended folder and add the reference to the main_extend.scss


### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-block-slide-css": "1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
